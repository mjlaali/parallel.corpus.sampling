package ca.concordia.clac.parallel.corpus.sampling.xml;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.conll2015.SyntaxReader;
import org.cleartk.discourse.type.DiscourseConnective;
import org.junit.Test;

import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseAnnotationFactory;
import ca.concordia.clac.uima.engines.LookupInstanceAnnotator;

public class LookupInstanceAnnotatorTest {

	@Test
	public void thenConnectivesAreAnnotated() throws UIMAException, IOException{
		JCas jcas = JCasFactory.createJCas();
		new SyntaxReader().initJCas(jcas, "(ROOT (S (NP (PRP It)) (VP (VBZ is) (NP (DT a) (NN test))) (. .)))");
		
		File tempFile = File.createTempFile("LookupInstanceAnnotator", ".txt");
		FileUtils.writeStringToFile(tempFile, "is\n", StandardCharsets.UTF_8);
		SimplePipeline.runPipeline(jcas, LookupInstanceAnnotator.getDescription(tempFile, DiscourseAnnotationFactory.class));
		
		Collection<DiscourseConnective> dcs = JCasUtil.select(jcas, DiscourseConnective.class);
		assertThat(dcs).hasSize(1);
		assertThat(dcs.iterator().next().getCoveredText()).isEqualTo("is");
	}
	
	@Test
	public void whenThereIsConnectiveThenRemoveTheOldOnes() throws UIMAException, IOException{
		JCas jcas = JCasFactory.createJCas();
		new SyntaxReader().initJCas(jcas, "(ROOT (S (NP (PRP It)) (VP (VBZ is) (NP (DT a) (NN test))) (. .)))");
		
		File tempFile = File.createTempFile("LookupInstanceAnnotator", ".txt");
		FileUtils.writeStringToFile(tempFile, "is\n", StandardCharsets.UTF_8);
		SimplePipeline.runPipeline(jcas, 
				LookupInstanceAnnotator.getDescription(tempFile, DiscourseAnnotationFactory.class), 
				LookupInstanceAnnotator.getDescription(tempFile, DiscourseAnnotationFactory.class));
		
		Collection<DiscourseConnective> dcs = JCasUtil.select(jcas, DiscourseConnective.class);
		assertThat(dcs).hasSize(1);
		assertThat(dcs.iterator().next().getCoveredText()).isEqualTo("is");
		
	}
}
