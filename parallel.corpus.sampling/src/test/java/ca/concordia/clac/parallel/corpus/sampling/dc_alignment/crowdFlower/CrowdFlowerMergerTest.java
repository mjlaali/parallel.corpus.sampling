package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.junit.Test;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerMerger;
import ca.concordia.clac.util.csv.CSVContent;

public class CrowdFlowerMergerTest {

	@Test
	public void givenATextThatContainDCwhenFixingAlignmentThenEscapeTheText() throws IOException{
		String frenchText = "Madame la Présidente, je voudrais savoir si cette";
		String f1 = "a,b,s\r\nsi	leila/alexis/andre	-1	-1	1.1098612288668108,8094,\"" + frenchText + "\"";
		
		CSVContent csvContent = new CSVContent(new StringReader(f1), "a", ',');

		CSVContent result = CrowdFlowerMerger.fixAlignmentIndexes(csvContent, "a", "b", "s");
		
		StringWriter output = new StringWriter();
		result.save(output);
		assertThat(output.toString()).isEqualTo(
				"a,b,s\r\nsi	leila/alexis/andre	8135	8137	1.1098612288668108,8094,\"" + frenchText + "\"\r\n");
	}
	
	@Test
	public void givenDCAtTheBegining() throws IOException{
		String frenchText = "Si the";
		String f1 = "a,b,s\r\nsi	leila/alexis/andre	-1	-1	1.1098612288668108,8094," + frenchText;
		
		CSVContent csvContent = new CSVContent(new StringReader(f1), "a", ',');

		CSVContent result = CrowdFlowerMerger.fixAlignmentIndexes(csvContent, "a", "b", "s");
		
		StringWriter output = new StringWriter();
		result.save(output);
		assertThat(output.toString()).isEqualTo(
				"a,b,s\r\nsi	leila/alexis/andre	8094	8096	1.1098612288668108,8094," + frenchText + "\r\n");
	}
	
	@Test
	public void givenDCAtTheEnd() throws IOException{
		String frenchText = "the si";
		String f1 = "a,b,s\r\nsi	leila/alexis/andre	-1	-1	1.1098612288668108,0," + frenchText;
		
		CSVContent csvContent = new CSVContent(new StringReader(f1), "a", ',');

		CSVContent result = CrowdFlowerMerger.fixAlignmentIndexes(csvContent, "a", "b", "s");
		
		StringWriter output = new StringWriter();
		result.save(output);
		assertThat(output.toString()).isEqualTo(
				"a,b,s\r\nsi	leila/alexis/andre	4	6	1.1098612288668108,0," + frenchText + "\r\n");
	}

	
	@Test
	public void givenDCWithSubscribe() throws IOException{
		String frenchText = "the que<sub>2</sub>,";
		String f1 = "a,b,s\r\nque2	leila/alexis/andre	-1	-1	1.1098612288668108,0,\"" + frenchText +"\"";
		
		CSVContent csvContent = new CSVContent(new StringReader(f1), "a", ',');

		CSVContent result = CrowdFlowerMerger.fixAlignmentIndexes(csvContent, "a", "b", "s");
		
		StringWriter output = new StringWriter();
		result.save(output);
		assertThat(output.toString()).isEqualTo(
				"a,b,s\r\nque	leila/alexis/andre	4	7	1.1098612288668108,0,\"" + frenchText + "\"\r\n");
	}

	
	@Test
	public void givenDCWithPunctuation() throws IOException{
		String frenchText = "the lorsqu' ,";
		
		String f1 = "a,b,s\r\nlorsqu'	leila/alexis/andre	-1	-1	1.1098612288668108,0,\"" + frenchText +"\"";
		
		CSVContent csvContent = new CSVContent(new StringReader(f1), "a", ',');

		CSVContent result = CrowdFlowerMerger.fixAlignmentIndexes(csvContent, "a", "b", "s");
		
		StringWriter output = new StringWriter();
		result.save(output);
		assertThat(output.toString()).isEqualTo(
				"a,b,s\r\nlorsqu'	leila/alexis/andre	4	11	1.1098612288668108,0,\"" + frenchText + "\"\r\n");
	}
	
	@Test
	public void givenDCAndAWord() throws IOException{
		String frenchText = "the s'il";
		String f1 = "a,b,s\r\ns'	leila/alexis/andre	-1	-1	1.1098612288668108,0," + frenchText +"";
		
		CSVContent csvContent = new CSVContent(new StringReader(f1), "a", ',');

		CSVContent result = CrowdFlowerMerger.fixAlignmentIndexes(csvContent, "a", "b", "s");
		
		StringWriter output = new StringWriter();
		result.save(output);
		assertThat(output.toString()).isEqualTo(
				"a,b,s\r\ns'	leila/alexis/andre	4	6	1.1098612288668108,0," + frenchText + "\r\n");
	}
	
	@Test
	public void givenSubscriptBeforeFrenchDc() throws IOException{
		String frenchText = "C'est<sub>1</sub> s'";
		String f1 = "a,b,s\r\ns'	leila/alexis/andre	-1	-1	1.1098612288668108,0,\"" + frenchText +"\"";
		
		CSVContent csvContent = new CSVContent(new StringReader(f1), "a", ',');
		
		CSVContent result = CrowdFlowerMerger.fixAlignmentIndexes(csvContent, "a", "b", "s");
		
		StringWriter output = new StringWriter();
		result.save(output);
		assertThat(output.toString()).isEqualTo(
				"a,b,s\r\ns'	leila/alexis/andre	6	8	1.1098612288668108,0," + frenchText + "\r\n");
	}


}
