package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.junit.Test;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.util.csv.CSVContent;

public class TestQuestionBuilderTest {
	
	@Test
	public void whenSelectingSubscriptedTextEndBoundary(){
		String subscriptedText = "It is<sub>1</sub> a test.";
		int begin = 3;
		int end = 5;
		
		String selected = TestQuestionBuilder.select(subscriptedText, begin, end);
		assertThat(selected).isEqualTo("is1");
	}
	
	
	@Test
	public void whenSelectingTextWithSubscript(){
		String subscriptedText = "It is<sub>123</sub> a test.";
		int begin = 3;
		int end = 7;
		
		String selected = TestQuestionBuilder.select(subscriptedText, begin, end);
		assertThat(selected).isEqualTo("is123 a");
	}
	
	@Test
	public void whenSelectingSubscriptedTextBeginBoundary(){
		String subscriptedText = "It'<sub>1</sub>s a test.";
		int begin = 3;
		int end = 4;
		
		String selected = TestQuestionBuilder.select(subscriptedText, begin, end);
		assertThat(selected).isEqualTo("s");
	}

	@Test
	public void whenMergingGoldAnnotations() throws IOException{
		String f = "srcGold,trgGold,srcBolded,trgBolded,srcBegin\r\n" +
					AlignmentFeature.convertToString(new AlignmentFeature("aussi", "andres/felix/leila/alexis/andre", 44212, 44217, 1.16094379124341)) + 
					",,here and <b>also</b> so that our roads are a good deal safer." +
					",juridique et <b>aussi</b> de sécurité sur nos routes.,43917";
		
		CSVContent csvContent = new CSVContent(new StringReader(f), "a", ',');
		
		StringWriter output = new StringWriter();
		
		new TestQuestionBuilder(csvContent).mergeGoldAlignmentsColumns("srcGold", "trgGold", "srcBolded", "trgBolded", "srcBegin");
		csvContent.save(output);

		String expected = "srcGold,trgGold,srcBolded,trgBolded,srcBegin\r\n" +
				AlignmentFeature.convertToString(new AlignmentFeature("aussi", "andres/felix/leila/alexis/andre", 44212, 44217, 1.16094379124341)) +
				"," + AlignmentFeature.convertToString(new AlignmentFeature("also", "andres/felix/leila/alexis/andre", 43917 + "here and ".length(), 43917 + "here and also".length(), 1.16094379124341)) +
				",here and <b>also</b> so that our roads are a good deal safer." +
				",juridique et <b>aussi</b> de sécurité sur nos routes.,43917\r\n";

		assertThat(output.toString()).isEqualTo(expected);
	}
	
	@Test
	public void whenSyncingGoldAnnotations() throws IOException{
		String f = "EN.dc.alignment.gold,FR.dc.alignment.gold,EN.chunk.bolded,FR.chunk.bolded,EN.chunk.begin,FR.chunk.begin\r\n" +
					AlignmentFeature.convertToString(new AlignmentFeature("aussi", "andres/felix/leila/alexis/andre", 44212, 44217, 1.16094379124341)) + 
					",,here and <b>also</b> so that our roads are a good deal safer." +
					",juridique et <b>aussi</b> de sécurité sur nos routes.,43917,";
		
		CSVContent csvContent = new CSVContent(new StringReader(f), "a", ',');
		
		StringWriter output = new StringWriter();
		
		new TestQuestionBuilder(csvContent).syncGoldAlignmentsColumns();
		csvContent.save(output);

		String expected = "EN.dc.alignment.gold,FR.dc.alignment.gold,EN.chunk.bolded,FR.chunk.bolded,EN.chunk.begin,FR.chunk.begin\r\n" +
				AlignmentFeature.convertToString(new AlignmentFeature("aussi", "andres/felix/leila/alexis/andre", 44212, 44217, 1.16094379124341)) +
				"," + AlignmentFeature.convertToString(new AlignmentFeature("also", "andres/felix/leila/alexis/andre", 43917 + "here and ".length(), 43917 + "here and also".length(), 1.16094379124341)) +
				",here and <b>also</b> so that our roads are a good deal safer." +
				",juridique et <b>aussi</b> de sécurité sur nos routes.,43917,\r\n";

		assertThat(output.toString()).isEqualTo(expected);
	}
}
