package ca.concordia.clac.parallel.corpus.sampling.xml;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.CasIOUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.corpus.europarl.type.Speaker;
import org.junit.Test;

import ca.concordia.clac.parallel.corpus.sampling.xml.CompleteEuroparlAnnotaion;

public class CompleteEuroparlAnnotationTest {

	@Test
	public void whenCompletingAnnotationThenOnlyMissingAnnotationsAreAdded() throws UIMAException, IOException{
		JCas aJCas = JCasFactory.createJCas();
		File aFile = new File("resources/sample/sample-annotations/ep-00-01-17.txt.xmi");
		
		assertThat(aFile).exists();
		CasIOUtil.readJCas(aJCas, aFile);
		
		JCas view = aJCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		int expectedParallelCount = JCasUtil.select(view, ParallelChunk.class).size();
		
		SimplePipeline.runPipeline(aJCas, CompleteEuroparlAnnotaion.getDescription());
		int outputParallelCount = JCasUtil.select(view, ParallelChunk.class).size();
		
		int outputSpeakerCount = JCasUtil.select(view, Speaker.class).size();
		assertThat(outputParallelCount).isEqualTo(expectedParallelCount);
		assertThat(outputSpeakerCount).isEqualTo(92 - 5);
	}
}
