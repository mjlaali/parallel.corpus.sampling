package ca.concordia.clac.parallel.corpus.sampling.dc_alignment;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.junit.Test;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.MergeAlignedRow;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.DcAlignmentsFeatures;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag;
import ca.concordia.clac.util.csv.CSVContent;

public class MergeAlignedRowTest {
	private final String dcHeader = Tag.tagToString(Tag.dc);
	private final String alignmentHeader = DcAlignmentsFeatures.ALIGNMENT_FEATURE_NAME;

	private String csvHeader(){
		return  
				Tag.addTags(dcHeader, Tag.EN, Tag.text) + "," +
				Tag.addTags(dcHeader, Tag.EN, Tag.begin) + "," +
				Tag.addTags(dcHeader, Tag.EN, Tag.end) + "," +
				Tag.addTags(alignmentHeader, Tag.EN, Tag.alignment, Tag.gold) + "," +
				Tag.addTags(dcHeader, Tag.FR, Tag.text) + "," +
				Tag.addTags(dcHeader, Tag.FR, Tag.begin) + "," +
				Tag.addTags(dcHeader, Tag.FR, Tag.end) + "," +
				Tag.addTags(alignmentHeader, Tag.FR, Tag.alignment, Tag.gold) + "\r\n"; 
	}

	private String addToCSV(String dc, Integer dcBegin, Integer dcEnd, String alignment, Integer alignmentBegin, Integer alignmentEnd){
		StringBuilder sb = new StringBuilder();

		int idx = 0;
		for (Tag tag: new Tag[]{Tag.text, Tag.begin, Tag.end, Tag.gold}){
			if (idx > 0)
				sb.append(",");
			switch (tag) {
			case text:
				if (dc != null)
					sb.append(dc);
				break;
			case begin:
				if (dcBegin != null)
					sb.append(dcBegin);
				break;
			case end:
				if (dcEnd != null)
					sb.append(dcEnd);
				break;
			case gold:
				if (alignment != null)
					sb.append(new AlignmentFeature(alignment, "alignment", alignmentBegin, alignmentEnd, 0.85).toString());
				break;

			default:
				break;
			}
			++idx;
		}
		return sb.toString();
	}

	private String addToCSV(Tag tLang, String dc, int dcBegin, int dcEnd, String alignment, int alignmentBegin, int alignmentEnd){
		StringBuilder sb = new StringBuilder();

		for (Tag lang: new Tag[]{Tag.EN, Tag.FR}){
			if (sb.length() > 0)
				sb.append(",");

			if (lang == tLang){
				sb.append(addToCSV(dc, dcBegin, dcEnd, alignment, alignmentBegin, alignmentEnd));
			} else {
				sb.append(addToCSV(null, null, null, null, null, null));
			}
		}
		sb.append("\r\n");
		return sb.toString();
	}



	@Test
	public void whenExtatlyMatched() throws IOException{
		String f1 = csvHeader() + 
				addToCSV(Tag.EN, "because", 5, 10, "pour", 2, 6) +
				addToCSV(Tag.FR, "pour", 2, 6, "because", 5, 10);

		CSVContent csv = new CSVContent(new StringReader(f1), "a", ',');

		StringWriter output = new StringWriter();

		MergeAlignedRow merger = new MergeAlignedRow();
		CSVContent results = merger.mergeAlignements(csv);

		results.save(output);
		assertThat(output.toString()).isEqualTo(
				csvHeader() + addToCSV("because", 5, 10, "pour", 2, 6) + "," + addToCSV("pour", 2, 6, "because", 5, 10) + "\r\n");
	}


	@Test
	public void whenDcCoversAlignment() throws IOException{
		String f1 =
				csvHeader() + 
				addToCSV(Tag.EN, "because", 5, 10, "pour", 2, 6) +
				addToCSV(Tag.FR, "pour", 2, 6, "because", 7, 9); 

		CSVContent csv = new CSVContent(new StringReader(f1), "a", ',');

		StringWriter output = new StringWriter();

		MergeAlignedRow merger = new MergeAlignedRow();
		CSVContent results = merger.mergeAlignements(csv);

		results.save(output);
		assertThat(output.toString()).isEqualTo(
				csvHeader() + addToCSV("because", 5, 10, "pour", 2, 6) + "," + addToCSV("pour", 2, 6, "because", 7, 9) + "\r\n");
	}


	@Test
	public void whenDcInsideAlignment() throws IOException{
		String f1 = 				
				csvHeader() + 
				addToCSV(Tag.EN, "because", 8, 8, "pour", 2, 6) +
				addToCSV(Tag.FR, "pour", 2, 6, "because", 7, 9); 
				;

		CSVContent csv = new CSVContent(new StringReader(f1), "a", ',');

		StringWriter output = new StringWriter();

		MergeAlignedRow merger = new MergeAlignedRow();
		CSVContent results = merger.mergeAlignements(csv);

		results.save(output);
		assertThat(output.toString()).isEqualTo(
				csvHeader() + addToCSV("because", 8, 8, "pour", 2, 6) + "," + addToCSV("pour", 2, 6, "because", 7, 9) + "\r\n");
	}
	
	@Test
	public void whenDcsAreNotAligned() throws IOException{
		String f1 = 				
				csvHeader() + 
				addToCSV(Tag.EN, "because", 8, 8, "pour", 2, 6) +
				addToCSV(Tag.FR, "pour", 6, 7, "because", 9, 9); 
		;
		
		CSVContent csv = new CSVContent(new StringReader(f1), "a", ',');
		
		StringWriter output = new StringWriter();
		
		MergeAlignedRow merger = new MergeAlignedRow();
		CSVContent results = merger.mergeAlignements(csv);
		
		results.save(output);
		String trimedOutput = output.toString().replace("\"\"", "");
		assertThat(trimedOutput).isEqualTo(
				f1);
	}
	
	@Test
	public void whenThereTwoDcThatAlignedRemovedBoth() throws IOException{
		String f1 = 				
				csvHeader() + 
				addToCSV(Tag.EN, "though", 15, 21, "s'il est vrai que", 36, 53) +
				addToCSV(Tag.FR, "s'", 36, 38, "", -1, -1) +  
				addToCSV(Tag.FR, "que", 50, 53, "", -1, -1); 
		;
		CSVContent csv = new CSVContent(new StringReader(f1), "a", ',');
		
		StringWriter output = new StringWriter();
		
		MergeAlignedRow merger = new MergeAlignedRow();
		CSVContent results = merger.mergeAlignements(csv);
		
		results.save(output);
		String trimedOutput = output.toString().replace("\"", "");
		assertThat(trimedOutput).isEqualTo(f1);
		
	}

}
