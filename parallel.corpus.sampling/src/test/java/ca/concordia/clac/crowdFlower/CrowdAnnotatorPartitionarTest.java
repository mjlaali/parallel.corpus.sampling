package ca.concordia.clac.crowdFlower;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class CrowdAnnotatorPartitionarTest {

	List<String> convertToString(Integer... ids){
		return Stream.of(ids).map((id) -> id.toString()).collect(Collectors.toList());
	}
	
	@Test
	public void whenEachRowDoneByDifferentAnnotatorsThenTheNumberOfRowsIsEqualToTheTotalNumberOfRows(){
		List<String> rowIds = convertToString(0, 0, 1, 1, 2, 2, 3, 3);
		List<String> annotatorIds = convertToString(0, 1, 2, 3, 4, 5, 6, 7);
		List<String> annotations = convertToString(0, 1, 2, 3, 4, 5, 6, 7);
		int groupCnt = 2;
		CrowdAnnotatorPartitionar crowdAnnotatorPartitionar = new CrowdAnnotatorPartitionar(rowIds, annotatorIds, annotations, groupCnt);
		
		assertThat(crowdAnnotatorPartitionar.getMissedRowCnt()).isEqualTo(0);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenThereIsNoConflictThenTheNumberOfRowsIsEqualToTheTotalNumberOfRows(){
		List<String> rowIds = convertToString(
				0, 0, 
				1, 1, 
				2, 2, 
				3, 3);
		List<String> annotatorIds = convertToString(
				0, 1, 
				0, 2, 
				3, 2, 
				3, 1);
		List<String> annotations = convertToString(0, 1, 2, 3, 4, 5, 6, 7);
		int groupCnt = 2;
		CrowdAnnotatorPartitionar crowdAnnotatorPartitionar = new CrowdAnnotatorPartitionar(rowIds, annotatorIds, annotations, groupCnt);
		
		assertThat(crowdAnnotatorPartitionar.getMissedRowCnt()).isEqualTo(0);
		assertThat(crowdAnnotatorPartitionar.getGroups()).containsOnly(
				new HashSet<>(Arrays.asList(new String[]{"0", "3"})),
				new HashSet<>(Arrays.asList(new String[]{"1", "2"})));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenThereIsConflictThenPickTheGroupWithLowesMissedRows(){
		List<String> rowIds = convertToString(
				0, 0, 
				1, 1, 
				2, 2, 
				3, 3,
				4, 4,
				5, 5,
				6, 6
				);
		List<String> annotatorIds = convertToString(
				0, 1, 
				0, 2, 
				0, 2, 
				3, 2,
				0, 3,
				0, 3,
				0, 3
				);
		List<String> annotations = convertToString(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
		int groupCnt = 2;
		CrowdAnnotatorPartitionar crowdAnnotatorPartitionar = new CrowdAnnotatorPartitionar(rowIds, annotatorIds, annotations, groupCnt);
		
		assertThat(crowdAnnotatorPartitionar.getMissedRowCnt()).isEqualTo(1);
		assertThat(crowdAnnotatorPartitionar.getGroups()).containsOnly(
				new HashSet<>(Arrays.asList(new String[]{"0"})),
				new HashSet<>(Arrays.asList(new String[]{"3", "2", "1"})));
	}

}
