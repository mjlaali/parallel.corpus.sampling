package ca.concordia.clac.pdtb.sampling;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.conll2015.ConllDatasetPath;
import org.cleartk.corpus.conll2015.ConllDatasetPath.DatasetMode;
import org.cleartk.corpus.conll2015.ConllDatasetPathFactory;
import org.cleartk.corpus.conll2015.ConllDiscourseGoldAnnotator;
import org.cleartk.corpus.conll2015.ConllSyntaxGoldAnnotator;
import org.cleartk.corpus.conll2015.TokenListTools;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.ml.Feature;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.json.FeatureJSonConverter;
import ca.concordia.clac.json.FeatureToCSVConverter;
import ca.concordia.clac.json.JSonDataWriter;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerFormater;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.FeatureJsonExtractor;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.JCasInitializable;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;

public class PDTBSampling implements Function<DiscourseConnective, List<Feature>>, JCasInitializable{
	private static final String CSV_FILE_NAME = "features.csv";
	private String docId;
	private Map<DiscourseConnective, Sentence> dcToSentence = new HashMap<>();
	private List<Sentence> sents;
	private CrowdFlowerFormater formater = new CrowdFlowerFormater();
	
	@Override
	public void init(JCas aJCas) {
		docId = DocumentMetaData.get(aJCas).getDocumentId();
		
		dcToSentence.clear();
		Map<DiscourseConnective, Collection<Sentence>> indexChunks = JCasUtil.indexCovering(aJCas, DiscourseConnective.class, Sentence.class);
		indexChunks.forEach((dc, s) -> dcToSentence.put(dc, s.iterator().next()));
		
		sents = new ArrayList<>(JCasUtil.select(aJCas, Sentence.class));
		Collections.sort(sents, (s1, s2) -> new Integer(s1.getBegin()).compareTo(s2.getBegin()));
	}
	
	@Override
	public List<Feature> apply(DiscourseConnective dc) {
		String[] sense = dc.getSense().split("\\.");
		String senseType = sense.length > 0 ? sense[0] : "";
		String senseClass = sense.length > 1 ? sense[1] : "";
		String senseSubClass = sense.length > 2 ? sense[2] : "";
		
		Sentence sent = dcToSentence.get(dc);
		String sentTxt = formater.makeAnnotationBold(sent, dc);
		
		int sentIdx = Collections.binarySearch(sents, sent, (s1, s2) -> new Integer(s1.getBegin()).compareTo(s2.getBegin()));
		String prevSentTxt = "";
		if (sentIdx != 0){
			prevSentTxt = sents.get(sentIdx - 1).getCoveredText();
		}
		return Arrays.asList(new Feature[]{
				new Feature("dc", TokenListTools.getTokenListText(dc).toLowerCase()),
				new Feature("type", senseType.toLowerCase()), 
				new Feature("class", senseClass.toLowerCase()), 
				new Feature("subclass", senseSubClass.toLowerCase()),
				new Feature("dc_begin", dc.getBegin()), 
				new Feature("docId", docId), 
				new Feature("sent", sentTxt), 
				new Feature("prevSent", prevSentTxt)
		});
	}
	
	public static AnalysisEngineDescription getDescription(File outputDir) throws ResourceInitializationException{
		return FeatureJsonExtractor.getDescription(outputDir, PDTBSampling.class);
	}
	
	public static void build(File datasetDir, File outputDir) throws UIMAException, IOException{
		outputDir.mkdirs();
		ConllDatasetPath dataset = new ConllDatasetPathFactory().makeADataset2016(datasetDir, DatasetMode.test);

		CollectionReaderDescription reader = CollectionReaderFactory.createReaderDescription(TextReader.class, 
				TextReader.PARAM_SOURCE_LOCATION, dataset.getRawDirectory(), 
				TextReader.PARAM_LANGUAGE, "en",
				TextReader.PARAM_PATTERNS, "wsj_*");
		AnalysisEngineDescription conllSyntaxJsonReader = 
				ConllSyntaxGoldAnnotator.getDescription(dataset.getParsesJSonFile());

		AnalysisEngineDescription conllGoldJsonReader = 
				ConllDiscourseGoldAnnotator.getDescription(dataset.getRelationsJSonFile());

		SimplePipeline.runPipeline(reader,
				conllSyntaxJsonReader, 
				conllGoldJsonReader, 
				getDescription(outputDir)
				);
		
		FeatureJSonConverter converter = new FeatureJSonConverter(new FeatureToCSVConverter(-1, new File(outputDir, CSV_FILE_NAME), false));
		converter.convert(new File(outputDir, JSonDataWriter.JSON_FILE_NAME));
	}
	
	interface Options{
		@Option(
				shortName = "i",
				longName = "inputDataset", 
				description = "Input dataset that contains all parsed files" )
		public File getInputDataset();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();
		
	}
	
	//-i sample/conll16st-en-01-12-16-trial
	//-o outputs/pdtb/trial
	public static void main(String[] args) throws UIMAException, IOException, URISyntaxException {
		Options options = CliFactory.parseArguments(Options.class, args);
		build(options.getInputDataset(), options.getOutputDir());
	}
}