package ca.concordia.clac.parallel.corpus.sampling.dc_alignment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.util.csv.CSVContent;
import ir.laali.tools.ds.DSManagment;

public class DatasetSampler {
	CSVContent sampled;
	Map<String, Integer> sampledCount = new HashMap<>();
	List<List<String>> csvContent;
	Map<String, Set<Integer>> frenchDcToRows;
	int maxDcSample;

	public DatasetSampler(CSVContent content, String frenchDcColumn, String goldAnnotationColumn, int maxDcSample) {
		this.maxDcSample = maxDcSample;
		sampled = new CSVContent(content.getHeader(), new ArrayList<>(), "sampled");
		csvContent = content.getContent();

		List<String> frenchDcs = content.getColumn(frenchDcColumn).stream().map(String::toLowerCase).collect(Collectors.toList());
		init(frenchDcs);

		addGoldAnnotation(content.getColumn(goldAnnotationColumn), frenchDcs);

		sample();

	}

	private void sample() {
		Random random = new Random(new Date().getTime());
		for (String frenchDc: new HashSet<>(sampledCount.keySet())){
			List<Integer> rows = new ArrayList<>(frenchDcToRows.get(frenchDc));
			for (int i = sampledCount.get(frenchDc); rows.size() > 0 && i < maxDcSample; i++){
				int selected = random.nextInt(rows.size());
				Integer row = rows.remove(selected);
				sampled.addRow(csvContent.get(row));
				DSManagment.incValue(sampledCount, frenchDc);
			}
		}
		System.err.println("DatasetSampler.sample(): total sampled data = " + sampled.getContent().size() + ", " + sampledCount.size());
		sampledCount.forEach((k, v) -> System.err.println(k + "\t" + v));
	}

	private void init(List<String> frenchDcs) {
		frenchDcToRows = new HashMap<>();
		for (int i = 0; i < frenchDcs.size(); i++){
			String frenchDc = frenchDcs.get(i);
			if (frenchDc.length() != 0){
				DSManagment.addToSet(frenchDcToRows, frenchDc, i);
				sampledCount.put(frenchDc, 0);
			}
		}
	}

	private void addGoldAnnotation(List<String> goldAnnotation, List<String> frenchDcs) {
		int goldAnnotationCnt = 0;
		for (int i = 0; i < goldAnnotation.size(); i++){
			String frenchDc = frenchDcs.get(i);
			if (goldAnnotation.get(i).length() != 0){
				if (frenchDc.length() != 0){	//there is a French dc with a gold annotation. 
					sampled.addRow(csvContent.get(i));
					frenchDcToRows.get(frenchDc).remove(i);
					DSManagment.incValue(sampledCount, frenchDc);
					goldAnnotationCnt++;
				} else {
					AlignmentFeature alignmentFeature = new AlignmentFeature(goldAnnotation.get(i));
					if (alignmentFeature.getText().length() > 0)
						System.err.println("Even the crowd aligned a dc to <" + goldAnnotation.get(i) + "> There is no French dc marked here!");
				}
			}
		}
		System.err.println("DatasetSampler.addGoldAnnotation(): Added gold annotation count = " + goldAnnotationCnt);
	}

	public CSVContent getSampled() {
		return sampled;
	}

	public static void main(String[] args) throws IOException {
		File datasetFile = new File("outputs/crowdFlower/newapproach/dataset-crowdFlower.csv");
		File outputFile = new File("outputs/crowdFlower/newapproach/sampled.csv");
		String frenchDcColumn = "FR.dc.text";
		String goldAnnotationColumn = "EN.dc.alignment.gold";
		int maxDcSample = 10;

		CSVContent sampled = new DatasetSampler(new CSVContent(datasetFile), frenchDcColumn, goldAnnotationColumn, maxDcSample).getSampled();
		sampled.save(outputFile);
	}
}
