package ca.concordia.clac.parallel.corpus.sampling.dc_alignment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.util.csv.CSVContent;

public class TestQuestionSampler {
	CSVContent dataset;
	CSVContent sampled;
	Random random = new Random(new Date().getTime());
	Set<Integer> selectedRows = new HashSet<>();
	
	public TestQuestionSampler(CSVContent dataset) {
		this.dataset = dataset;
		this.sampled = new CSVContent(dataset.getHeader(), new ArrayList<>(), "test-questions");
	}
	
	public TestQuestionSampler addZeroAlignmentDc(String frenchAlignmentColumn, int cnt){
		List<List<AlignmentFeature>> rowAlignments = dataset.getColumn(frenchAlignmentColumn).stream().map(AlignmentFeature::parse).collect(Collectors.toList());
		
		List<Integer> zeroAlignmentRows = filter(rowAlignments, (al) -> al.size() == 1 && al.get(0).getBegin() == -1);
		add(zeroAlignmentRows, cnt);
		return this;
	}
	
	public <T> List<Integer> filter(List<T> column, Predicate<T> predicate){
		List<Integer> acceptedRows = new ArrayList<>();
		int idx = 0; 
		for (T cell: column){
			
			if (predicate.test(cell)){
				acceptedRows.add(idx);
			}
			idx++;
		}
		return acceptedRows;
	}
	
	public TestQuestionSampler removeRowWithAnnotations(String goldAlignmentColumn){
		List<List<AlignmentFeature>> goldAlignments = dataset.getColumn(goldAlignmentColumn).stream().map(AlignmentFeature::parse).collect(Collectors.toList());
		List<Integer> rowWithGoldAnnotations = filter(goldAlignments, (a) -> a.size() != 0);
		selectedRows.addAll(rowWithGoldAnnotations);
		return this;
	}
	
	public TestQuestionSampler addNonDc(String senseColumn, int cnt){
		List<String> senses = dataset.getColumn(senseColumn);
		List<Integer> nonDcRows = filter(senses, (s) -> s.equals(Boolean.toString(false)));
		add(nonDcRows, cnt);
		return this;
	}

	private void add(List<Integer> rows, int cnt) {
		rows.removeAll(selectedRows);
		for (int i = 0; i < cnt && rows.size() > 0; i++){
			Integer selectedRow = rows.remove(random.nextInt(rows.size()));
			sampled.addRow(dataset.getContent().get(selectedRow));
			selectedRows.add(selectedRow);
		}
	}
	
	public CSVContent getSampled() {
		return sampled;
	}

	public static void main(String[] args) throws IOException {
		File datasetFile = new File("outputs/crowdFlower/newapproach/sampled.csv");
		File outputFile = new File("outputs/crowdFlower/newapproach/test.csv");
		CSVContent dataset = new CSVContent(datasetFile);
		new TestQuestionSampler(dataset)
			.removeRowWithAnnotations("EN.dc.alignment.gold")
			.addZeroAlignmentDc("FR.dc.alignment", 16)
			.addNonDc("FR.dc.sense", 10)
			.getSampled().save(outputFile);
	}
}
