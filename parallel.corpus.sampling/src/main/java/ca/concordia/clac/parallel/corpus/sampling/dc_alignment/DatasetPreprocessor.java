package ca.concordia.clac.parallel.corpus.sampling.dc_alignment;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.uima.UIMAException;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.batch_process.BatchProcess;
import ca.concordia.clac.discourse.FrConnectiveClassifier;

public class DatasetPreprocessor {
	interface Options{
		@Option(
				shortName = "i",
				longName = "inputDataset", 
				description = "Input dataset that contains all parsed files" )
		public File getInputDataset();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();
		
		@Option(
				shortName = "m",
				longName = "modelDir",
				description = "Specify the model directory of the French DC classifier.")
		public File getFrenchModelDir();

	}
	
	/*-i /Users/majid/Documents/git/disco-parallel/parallel.corpus/resources/test/small-sample-word-aligned/A3.final 
	 * -o outputs/crowdflower/newapproach/xmi 
	 * -m /Users/majid/Documents/git/french-connective-disambiguation/connective-disambiguation/outputs/fdtb-gold/model
	 */
	
	public static void main(String[] args) throws URISyntaxException, UIMAException, IOException, SAXException, CpeDescriptorException {
		Options options = CliFactory.parseArguments(Options.class, args);
		
		CollectionReaderDescription inputReaderDescription = BatchProcess.getXmiReader(options.getInputDataset());
		BatchProcess process = new BatchProcess(inputReaderDescription, options.getOutputDir());
		process.addProcess("french_parser", EuroparlParallelTextAnnotator.FR_TEXT_VIEW, 
				FrConnectiveClassifier.getClassifierDescription(new File(options.getFrenchModelDir(), "model.jar").toURI().toURL()));
		process.run();
		
	}
}
