package ca.concordia.clac.parallel.corpus.sampling.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

public class MergeParallelXML {
	private Map<String, Element> offsetToFrNode; 
	private String tagname;
	private String attrName;

	private Document enDoc;
	private Document frDoc;

	private Transformer trans;
	private Set<String> filterAttributes = new HashSet<>(Arrays.asList("translation", "tokens", "comment", "wrappedAnnotation", "discourseRelation"));

	public MergeParallelXML(File enFile, File frFile, String tagname, String attrName) throws SAXException, IOException, ParserConfigurationException, TransformerConfigurationException {
		this.tagname = tagname;
		this.attrName = attrName;

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		enDoc = dBuilder.parse(enFile);
		frDoc = dBuilder.parse(frFile);

		TransformerFactory tFact = TransformerFactory.newInstance();
		trans = tFact.newTransformer();

		NodeList nList = frDoc.getElementsByTagName(tagname);
		offsetToFrNode = new HashMap<>();

		for (int i = 0; i < nList.getLength(); i++){
			Element element = (Element) nList.item(i);
			offsetToFrNode.put(element.getAttribute(attrName), element);
		}

		Element root = enDoc.getDocumentElement();
		dfs(root);

	}

	public void saveTo(File output) throws TransformerException, FileNotFoundException {
		PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(output), StandardCharsets.UTF_8));
		saveXML(writer, enDoc);
	}

	private void dfs(Element element) {
		if (element.getTagName().equals(tagname)){
			String docOffset = element.getAttribute(attrName);
			Element frElement = offsetToFrNode.get(docOffset);
			merge(element, frElement);
		} else {
			NodeList childNodes = element.getChildNodes();
			for (int i = 0; i < childNodes.getLength(); i++){
				Node n = childNodes.item(i);
				if (n instanceof Element)
					dfs((Element) n);
			}
		}
	}

	private void merge(Element element, Element frElement) {
		if (element == null || frElement == null)	//if either of input is null, it means that the other one is not aligned to anything. So we do not include it in the final xml file.
			return;
		NodeList nList = element.getChildNodes();
		Element newEnElement = enDoc.createElement("en");
		Element newFrElement = enDoc.createElement("fr");


		for (int i = 0; i < nList.getLength(); i++){
			Node n = nList.item(i);
			Node copy = n.cloneNode(true); 
			newEnElement.appendChild(copy);
		}

		while (nList.getLength() > 0){
			element.removeChild(nList.item(0));
		}

		nList = frElement.getChildNodes();
		for (int i = 0; i < nList.getLength(); i++){
			Node n = nList.item(i).cloneNode(true);
			enDoc.adoptNode(n);
			newFrElement.appendChild(n);
		}

		element.appendChild(newEnElement);
		element.appendChild(newFrElement);

	}

	private void dfsRemoveAttributes(Element anElement){
		for (String attr: filterAttributes){
			anElement.removeAttribute(attr);
		}
		
		if (anElement.getAttribute("alignment").contains("Alignment-")){
			anElement.setAttribute("alignment", anElement.getAttribute("alignment").replace("Alignment-", ""));
		}

		NodeList childNodes = anElement.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++){
			Node n = childNodes.item(i);
			if (n instanceof Element)
				dfsRemoveAttributes((Element) n);
		}

	}

	private void saveXML(PrintWriter output, Document doc) throws TransformerException {

		StreamResult result = new StreamResult(output);
		dfsRemoveAttributes(doc.getDocumentElement());
		DOMSource source = new DOMSource(doc);
		
		ProcessingInstruction pi = doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"translator.xsl\"");
		doc.insertBefore(pi, doc.getDocumentElement());
		trans.setOutputProperty(OutputKeys.INDENT, "yes");
		trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		try {
			trans.transform(source, result);

		} finally {
			output.close();
		}
	}

	interface Options{
		@Option(
				shortName = "i",
				longName = "inputDataset", 
				description = "Input dataset that contains all parsed files" )
		public File getInputDataset();

		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();
	}

	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, TransformerException {
		Options options = CliFactory.parseArguments(Options.class, args);

		List<File> allFiles = Arrays.asList(options.getInputDataset().listFiles());
		Map<String, List<File>> remainingFiles = new HashMap<>();
		for (File aFile: allFiles){
			if (aFile.getName().contains("-en")){
				String name = aFile.getName().replace("-en", "");
				List<File> fileList = new ArrayList<>();
				fileList.add(aFile);
				remainingFiles.put(name, fileList);
			}
		}

		for (File aFile: allFiles){
			if (aFile.getName().contains("-fr")){
				String name = aFile.getName().replace("-fr", "");
				remainingFiles.get(name).add(aFile);
			}
		}

		if (options.getOutputDir().exists())
			FileUtils.deleteDirectory(options.getOutputDir());
		
		options.getOutputDir().mkdirs();
		for (Entry<String, List<File>> aFileToLanguageFiles: remainingFiles.entrySet()){
			File enFile = aFileToLanguageFiles.getValue().get(0);
			File frFile = aFileToLanguageFiles.getValue().get(1);

			String tagname = "ParallelChunk";
			String attrName = "docOffset";

			File output = new File(options.getOutputDir(), aFileToLanguageFiles.getKey());
			new MergeParallelXML(enFile, frFile, tagname, attrName).saveTo(output);
		}

		System.out.println("MergeParallelXML.main()");
	}


}
