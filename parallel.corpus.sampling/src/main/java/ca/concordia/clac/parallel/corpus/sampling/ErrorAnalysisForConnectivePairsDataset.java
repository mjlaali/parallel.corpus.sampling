package ca.concordia.clac.parallel.corpus.sampling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.math3.util.Pair;
import org.apache.uima.UIMAException;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.ml.Feature;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ca.concordia.clac.json.JSonDataWriter;
import ca.concordia.clac.ml.classifier.GenericClassifierLabeller;
import ca.concordia.clac.parallel.corpus.evaluation.lexicon.ParallelConnectives;
import ca.concordia.clac.parallel.corpus.moses.phrase_table.SenseRemoval;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerFormater;
import ca.concordia.clac.parallel.corpus.sampling.xml.XMLDataset;
import ca.concordia.clac.uima.Utils;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;

public class ErrorAnalysisForConnectivePairsDataset extends ExportConnectiveInstancesBase {
	public static final String PARAM_TARGET_SENSE = "targetSense";
	public static final String PARAM_TARGET_EN_DCS = "targetEnDcs";
	public static final String PARAM_TARGET_FR_DCS = "targetFrDcs";
	private Function<String, String> normalizer = (str) -> str.toLowerCase();
	private Function<String, String> senseNormalizer = SenseRemoval.relationNormalizer;
	private CrowdFlowerFormater formater = new CrowdFlowerFormater();

	private Set<String> validEnDcs, validFrDcs;


	@ConfigurationParameter(name=PARAM_TARGET_SENSE)
	private String targetSense;

	@ConfigurationParameter(name=PARAM_TARGET_EN_DCS)
	private String[] targetEnDcs;

	@ConfigurationParameter(name=PARAM_TARGET_FR_DCS)
	private String[] targetFrDcs;

	protected Pair<DiscourseConnective, DiscourseConnective> accept(DiscourseConnective enDc, DiscourseConnective frDc) {
		String sense = senseNormalizer.apply(enDc.getSense());
		String enDcText = normalizer.apply(enDc.getCoveredText());
		String frDcText = normalizer.apply(frDc.getCoveredText());
		if (sense.equals(senseNormalizer.apply(targetSense)) && validEnDcs.contains(enDcText) && validFrDcs.contains(frDcText)){
			return new Pair<>(enDc, frDc);
		}
		return null;
	}

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		validEnDcs = new HashSet<>(Arrays.asList(targetEnDcs));
		validFrDcs = new HashSet<>(Arrays.asList(targetFrDcs));
	}

	public static AnalysisEngineDescription getEngineDescription(File outputDir, String sense, String[] targetEnDcs, String[] targetFrDcs) throws ResourceInitializationException{
		return GenericClassifierLabeller.getWriterDescription(ExportConnectiveInstancesBase.class, JSonDataWriter.class, outputDir, 
				PARAM_TARGET_SENSE, sense,
				PARAM_TARGET_EN_DCS, targetEnDcs, 
				PARAM_TARGET_FR_DCS, targetFrDcs);
	}
	
	protected List<Feature> getFeatures(String docId, Pair<DiscourseConnective, DiscourseConnective> pair,
			ParallelChunk enChunk) {
		String enText = formater.makeAnnotationBold(enChunk, pair.getFirst());
		String frText = formater.makeAnnotationBold(enChunk.getTranslation(), pair.getSecond());

		return Arrays.asList(new Feature[]{
				new Feature("enDc", pair.getFirst().getCoveredText()),
				new Feature("sense", pair.getFirst().getSense()),
				new Feature("en", enText), 
				new Feature("fr", frText), 
				new Feature("sent_id", enChunk.getDocOffset()),
				new Feature("doc_id", docId),
				});
	}

	public static void main(String[] args) throws UIMAException, IOException, ParserConfigurationException, SAXException {
//		File datasetFld = new File("/Users/majid/Documents/git/parallel.corpus.sampling/parallel.corpus.sampling/sample/sample-annotations");
		File datasetFld = new File("/Users/majid/Documents/resource/discourseParsing-fr");
		File lexconnFile = new File("/Users/majid/Documents/git/french-connective-disambiguation/connective-disambiguation/data/lexconn_V2.1/Lexconn_v2.1.xml");
		File connectivePairFiles = new File("/Users/majid/Documents/git/disco-parallel/parallel.corpus/outputs/lexicon/en-discourse-usage/suggested-simple-mapping.json");
//		File connectivePairFiles = new File("/Users/majid/Documents/git/disco-parallel/parallel.corpus/outputs/lexicon/en-discourse-usage/suggested-leave-one-out.json");
//		File connectivePairFiles = new File("/Users/majid/Documents/git/disco-parallel/parallel.corpus/outputs/lexicon/en-discourse-usage/intersection-leave-one-out.json");
//		Set<String> targetPDTBRelations = new HashSet<>(Arrays.asList("Expansion.Alternative.Chosen-Alternative"));
//		Set<String> targetLexconnRelations = new HashSet<>(Arrays.asList("opposition"));

		File outputDir = new File("outputs/simple-mapping-all-europarl/");
		outputDir.mkdirs();
		
		File tempFile = File.createTempFile(ExportConnectiveInstancesBase.class.getName(), ".txt");
		AggregateBuilder builder = setupExtractors(lexconnFile, connectivePairFiles, outputDir, tempFile);

		CollectionReader reader = CollectionReaderFactory.createReader(XmiReader.class, 
				XmiReader.PARAM_SOURCE_LOCATION, datasetFld, 
				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
		
		System.out.println("Start extracting instancs...");

		try{
			Utils.runWithProgressbar(reader, builder.createAggregateDescription());
		} finally{
			tempFile.delete();
		}
	}

	private static AggregateBuilder setupExtractors(File lexconnFile, File connectivePairFiles, File outputDir,
			File tempFile) throws FileNotFoundException, IOException, ParserConfigurationException, SAXException,
			ResourceInitializationException {
		Gson gson = new GsonBuilder().create();
		FileReader connectivePairReader = new FileReader(connectivePairFiles);
		ParallelConnectives[] fromJson = gson.fromJson(connectivePairReader, ParallelConnectives[].class);
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(XMLDataset.frDCAnnotator(lexconnFile, tempFile), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);

		System.out.println("Start adding extractors ...");
		
		NumberFormat formatter = new DecimalFormat("#0.000");
		int cnt = 0;
		for (ParallelConnectives pair: fromJson){
			if (pair.getProb() < 0.0)
				continue;
			String pdtbRelation = pair.getPdtbRelation();
			String lexconnRelation = pair.getLexconnRelation();
//			if (targetPDTBRelations.contains(pdtbRelation) && targetLexconnRelations.contains(lexconnRelation)){
				System.out.println(cnt++ + pair.getFrDcs().toString());
				String[] targetEnDcs = pair.getEnDcs().toArray(new String[0]);		
				String[] targetFrDcs = pair.getFrDcs().toArray(new String[0]);

				File pairOutput = new File(outputDir, formatter.format(pair.getProb()) + "-" + targetFrDcs[0] + "-"  + pdtbRelation + "-" + lexconnRelation);
				pairOutput.mkdirs();
				builder.add(getEngineDescription(pairOutput, pdtbRelation, targetEnDcs, targetFrDcs));
//			}
		}
		return builder;
	}

	
}
