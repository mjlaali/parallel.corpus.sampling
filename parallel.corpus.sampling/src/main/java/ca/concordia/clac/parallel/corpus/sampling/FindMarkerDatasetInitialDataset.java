package ca.concordia.clac.parallel.corpus.sampling;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.ml.Feature;

import ca.concordia.clac.json.FeatureJSonConverter;
import ca.concordia.clac.json.FeatureToCSVConverter;
import ca.concordia.clac.json.JSonDataWriter;
import ca.concordia.clac.ml.classifier.GenericClassifierLabeller;
import ca.concordia.clac.ml.classifier.InstanceExtractor;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerFormater;
import ca.concordia.clac.uima.Utils;
import ca.concordia.clac.uima.types.paralleltexts.Alignment;
import ca.concordia.clac.util.csv.CSVContent;
import ca.concordia.clac.util.csv.CSVMerger;
import ca.concordia.clac.util.csv.MergeStrategy;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;

/**
 * This class generate initial dataset for the FindMarker task in CrowdFlower. Note that you should add test annotations to 
 * the dataset by using the {@link FindMarkerDataset} class. 
 * @author majid
 *
 */
public class FindMarkerDatasetInitialDataset extends ExportConnectiveInstancesBase{
	Set<DiscourseConnective> seenConnectives = new HashSet<>();
	Map<ParallelChunk, Collection<Token>> chunkToWords;
	Map<Token, Collection<Alignment>> tokenAlignments;
	CrowdFlowerFormater crowdFlowerFormater = new CrowdFlowerFormater();

	@Override
	public InstanceExtractor<Pair<DiscourseConnective, DiscourseConnective>> getExtractor(JCas jCas) {
		InstanceExtractor<Pair<DiscourseConnective, DiscourseConnective>> extractor = super.getExtractor(jCas);
		seenConnectives.clear();
		if (JCasUtil.select(enView, Alignment.class).size() == 0)
			throw new RuntimeException("Please add word alignment before running this process.");
		chunkToWords = JCasUtil.indexCovered(frView, ParallelChunk.class, Token.class);
		tokenAlignments = JCasUtil.indexCovering(enView, Token.class, Alignment.class);
		return extractor;
	}

	@Override
	protected Pair<DiscourseConnective, DiscourseConnective> accept(DiscourseConnective enDc, DiscourseConnective frDc) {
		if (seenConnectives.contains(enDc))
			return null;
		seenConnectives.add(enDc);
		return new Pair<>(enDc, frDc);
	}


	@Override
	protected List<Feature> getFeatures(String docId, Pair<DiscourseConnective, DiscourseConnective> pair,
			ParallelChunk enChunk) {
		DiscourseConnective enDc = pair.getFirst();
		String enText = crowdFlowerFormater.makeAnnotationBold(enChunk, enDc);
		ParallelChunk frChunk = enChunk.getTranslation();
		String frText = crowdFlowerFormater.putSubscriptForDuplicateWords(frChunk);
		
		String alignment = getAlignment(enDc);

		return Arrays.asList(new Feature[]{
				new Feature("en", enText), 
				new Feature("fr", frText), 
				new Feature("enDc", enDc.getCoveredText()),
				new Feature("sense", enDc.getSense()),
				new Feature("sent_id", enChunk.getDocOffset()),
				new Feature("doc_id", docId),
				new Feature("dc_id", enDc.getBegin()),
				new Feature("word_alignment", alignment)
		});
	}

	private String getAlignment(DiscourseConnective enDc) {
		Set<Token> tokenSet = new HashSet<>();
		for (int i = 0; i < enDc.getTokens().size(); i++){
			for (Alignment alignment: tokenAlignments.get(enDc.getTokens(i))){
				tokenSet.add((Token)alignment.getAlignment().getWrappedAnnotation());
			}
		}
		
		List<Token> tokens = new ArrayList<>(tokenSet);
		Collections.sort(tokens, (t1, t2) -> new Integer(t1.getBegin()).compareTo(t2.getBegin()));
		String alignment = tokens.stream().map(Token::getCoveredText).collect(Collectors.joining(" "));
		return alignment;
	}

	public static AnalysisEngineDescription getEngineDescription(File outputDir) throws ResourceInitializationException{
		return GenericClassifierLabeller.getWriterDescription(FindMarkerDatasetInitialDataset.class, JSonDataWriter.class, outputDir);
	}


	public void build(File outputDir, File datasetFld, File... toBeAdded) throws UIMAException, IOException{
		outputDir.mkdirs();
		File csvFile = new File(outputDir, "dataset.csv");
		File jsonFile = new File(outputDir, JSonDataWriter.JSON_FILE_NAME);
		AnalysisEngineDescription engineDescription = getEngineDescription(outputDir);

		CollectionReader reader = CollectionReaderFactory.createReader(XmiReader.class, 
				XmiReader.PARAM_SOURCE_LOCATION, datasetFld, 
				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
		Utils.runWithProgressbar(reader, engineDescription);
		
		FeatureToCSVConverter featureJSonToCsv = new FeatureToCSVConverter(-1, csvFile, false);
		new FeatureJSonConverter(featureJSonToCsv).convert(jsonFile);
		jsonFile.delete();
	}

	public static void main(String[] args) throws UIMAException, IOException, InterruptedException {
		File datasetFlds = new File("/Users/majid/Documents/git/disco-parallel/parallel.corpus/resources/test/small-sample-word-aligned");
		File baseOutput = new File("resources/crowdFlower/word-alignment");
		baseOutput.mkdirs();
		for (File datasetFld: datasetFlds.listFiles()){
			File outputDir = new File(baseOutput, datasetFld.getName());
			File aggregatedReport = new File("resources/crowdFlower/10dollar/aggregated.csv");
			File testQuestions = new File("resources/crowdFlower/10dollar/test-questions.csv");
			new FindMarkerDatasetInitialDataset().build(outputDir, datasetFld, aggregatedReport, testQuestions);
		}
		
		System.err.println("FindMarkerDatasetInitialDataset.main(): Start merging word alignments");
		
		String keyColumn = "en";
		MergeStrategy mergeStrategy = (a, b, k) -> a.isEmpty() ? b : a;
		CSVMerger merger = new CSVMerger(keyColumn, mergeStrategy);
		
		for (File alignmentFld: baseOutput.listFiles()){
			if (!alignmentFld.isDirectory())
				continue;
			File csv2File = new File(alignmentFld, "dataset.csv");
			CSVContent csv2 = new CSVContent(csv2File);
			csv2.renameColumn("word_alignment", alignmentFld.getName());
			merger.merge(csv2);
		}
		
		merger.getCsvContent().save(new File(baseOutput, "dataset-merged.csv"));
		System.out.println("CSVMerger.main()");
		
	}
}
