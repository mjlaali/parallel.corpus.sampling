package ca.concordia.clac.parallel.corpus.sampling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.math3.util.Pair;
import org.apache.uima.UimaContext;
import org.apache.uima.cas.CASException;
import org.apache.uima.fit.component.initialize.ConfigurationParameterInitializer;
import org.apache.uima.fit.component.initialize.ExternalResourceInitializer;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.ml.Feature;

import ca.concordia.clac.ml.classifier.ClassifierAlgorithmFactory;
import ca.concordia.clac.ml.classifier.InstanceExtractor;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;

public abstract class ExportConnectiveInstancesBase implements ClassifierAlgorithmFactory<String, Pair<DiscourseConnective, DiscourseConnective>>{
	protected JCas enView, frView;
	private Map<DiscourseConnective, ParallelChunk> enDcToParallelChunk;

	public class ConnectiveSelecter implements InstanceExtractor<Pair<DiscourseConnective, DiscourseConnective>>{

		@Override
		public Collection<Pair<DiscourseConnective, DiscourseConnective>> getInstances(JCas container) {
			Map<ParallelChunk, Collection<DiscourseConnective>> parallelChunkToFrDc = JCasUtil.indexCovered(frView, ParallelChunk.class, DiscourseConnective.class);

			List<Pair<DiscourseConnective, DiscourseConnective>> pairConenctives = new ArrayList<>();

			for (Entry<DiscourseConnective, ParallelChunk> anEntry: enDcToParallelChunk.entrySet()){
				DiscourseConnective enDc = anEntry.getKey();
				ParallelChunk enChunk = anEntry.getValue();
				ParallelChunk frChunk = enChunk.getTranslation();
				Collection<DiscourseConnective> frConnectives = parallelChunkToFrDc.get(frChunk);
				if (frConnectives != null && frConnectives.size() > 0){
					for (DiscourseConnective frDc: frConnectives){
						Pair<DiscourseConnective, DiscourseConnective> pair = accept(enDc, frDc);
						if (pair != null)
							pairConenctives.add(pair);
					}
				} else {
					Pair<DiscourseConnective, DiscourseConnective> pair = accept(enDc, null);
					if (pair != null)
						pairConenctives.add(pair);
				}
					
				
			}
			return pairConenctives;
		}
	}
	
	protected abstract Pair<DiscourseConnective, DiscourseConnective> accept(DiscourseConnective enDc, DiscourseConnective frDc);

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		ConfigurationParameterInitializer.initialize(this, context);
		ExternalResourceInitializer.initialize(this, context);
	}

	private Map<DiscourseConnective, ParallelChunk> getDcToParallelChunkMap(JCas aView){
		Map<DiscourseConnective, ParallelChunk> map = new HashMap<>();
		JCasUtil.indexCovering(aView, DiscourseConnective.class, ParallelChunk.class).forEach((dc, chunks) -> map.put(dc, chunks.iterator().next()));
		return map;
	}

	@Override
	public InstanceExtractor<Pair<DiscourseConnective, DiscourseConnective>> getExtractor(JCas jCas) {
		try {
			enView = jCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
			enDcToParallelChunk = getDcToParallelChunkMap(enView);
			frView = jCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		} catch (CASException e) {
			throw new RuntimeException(e);
		}
		return new ConnectiveSelecter();
	}

	@Override
	public List<Function<Pair<DiscourseConnective, DiscourseConnective>, List<Feature>>> getFeatureExtractor(JCas jCas) {
		String docId = DocumentMetaData.get(jCas).getDocumentId();
		Function<Pair<DiscourseConnective, DiscourseConnective>, List<Feature>> features = (pair) -> {
			ParallelChunk enChunk = enDcToParallelChunk.get(pair.getFirst());
			
			return getFeatures(docId, pair, enChunk);
		};
		return Arrays.asList(features);
	}

	protected abstract List<Feature> getFeatures(String docId, Pair<DiscourseConnective, DiscourseConnective> pair,
			ParallelChunk enChunk) ;


	@Override
	public Function<Pair<DiscourseConnective, DiscourseConnective>, String> getLabelExtractor(JCas jCas) {
		return (pair) -> "" + pair.getFirst().getBegin() + "_" + Optional.ofNullable(pair.getSecond()).map(DiscourseConnective::getBegin).orElse(null);
	}

	@Override
	public BiConsumer<String, Pair<DiscourseConnective, DiscourseConnective>> getLabeller(JCas jCas) {
		return null;
	}

	

}
