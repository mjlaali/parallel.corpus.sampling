package ca.concordia.clac.parallel.corpus.sampling.xml;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseAnnotationFactory;
import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.parallel.corpus.builder.AnnotationProjectionFilter;
import ca.concordia.clac.uima.engines.LookupInstanceAnnotator;
import ca.concordia.clac.uima.engines.XMLGenerator;
import ca.concordia.clac.uima.types.paralleltexts.Alignment;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;

/**
 * Build xml files for each languages. These files can be combined later using {@link MergeParallelXML}.
 * The input xmi files should have already contained annotations of English discourse connectives and their alignments to French discourse connectives.
 * @author majid
 *
 */
public class XMLDataset {
	public static class FilterWordAlignments extends JCasAnnotator_ImplBase{

		@Override
		public void process(JCas aJCas) throws AnalysisEngineProcessException {
			Collection<Alignment> alignments = new ArrayList<>(JCasUtil.select(aJCas, Alignment.class));
			for (Alignment alignment: alignments){
				if (!alignment.getComment().equals(AnnotationProjectionFilter.ALIGNMENT_COMMENT)){
					alignment.removeFromIndexes();
				}
			}
		}
		
		public static AnalysisEngineDescription getDescription() throws ResourceInitializationException {
			return createEngineDescription(FilterWordAlignments.class);
		}
		
	}

	public XMLDataset(File datasetFld, File outDir, String... types) throws UIMAException, IOException, SAXException, CpeDescriptorException, ParserConfigurationException {
		CollectionReaderDescription reader = CollectionReaderFactory.createReaderDescription(XmiReader.class, 
				XmiReader.PARAM_SOURCE_LOCATION, datasetFld, 
				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});

		AggregateBuilder builder = new AggregateBuilder();
		builder.add(CompleteEuroparlAnnotaion.getDescription());
		
		builder.add(FilterWordAlignments.getDescription(), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		builder.add(FilterWordAlignments.getDescription(), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		
		builder.add(XMLGenerator.getDescription(outDir, "-en.xml", true, types), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		builder.add(XMLGenerator.getDescription(outDir, "-fr.xml", true, types), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		
		SimplePipeline.runPipeline(reader, builder.createAggregateDescription());
	}


	public static AnalysisEngineDescription frDCAnnotator(File lexconnFile, File tempFile) throws IOException, ParserConfigurationException, SAXException, ResourceInitializationException {
		
		DefaultLexconnReader lexconn = DefaultLexconnReader.getLexconnMap(lexconnFile);
		FileUtils.writeLines(tempFile, new ArrayList<>(lexconn.getFormToId().keySet()));
		return LookupInstanceAnnotator.getDescription(tempFile, DiscourseAnnotationFactory.class);
	}
	
	interface Options{
		@Option(
				shortName = "i",
				longName = "inputDataset", 
				description = "Input dataset that contains all parsed files" )
		public File getInputDataset();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();
	}
	
	public static void main(String[] args) throws UIMAException, IOException, SAXException, CpeDescriptorException, ParserConfigurationException {
		Options options = CliFactory.parseArguments(Options.class, args);
		File datasetFld = options.getInputDataset();
		
		File outDir =  options.getOutputDir();
		FileUtils.deleteDirectory(outDir);
		outDir.mkdirs();
		
		new XMLDataset(datasetFld, outDir, "Speaker", "ParallelChunk", "Alignment", "DiscourseConnective");
	}
}
