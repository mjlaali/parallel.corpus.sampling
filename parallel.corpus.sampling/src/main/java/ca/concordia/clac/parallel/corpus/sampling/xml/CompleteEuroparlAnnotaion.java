package ca.concordia.clac.parallel.corpus.sampling.xml;

import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CASException;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;

/**
 * Add missing annotations to Europarl text. Current the old version of the dataset does not contains some of Europalr annotaitons. 
 * This class adds those annotations (e.g. Speaker, P, etc.).
 * @author majid
 *
 */
public class CompleteEuroparlAnnotaion extends EuroparlParallelTextAnnotator{
	public static AnalysisEngineDescription getDescription() throws ResourceInitializationException {
		return AnalysisEngineFactory.createEngineDescription(CompleteEuroparlAnnotaion.class);
	}

	@Override
	protected void align(JCas aJCas) throws CASException {
		super.align(aJCas);
	}
	
	@Override
	protected JCas createView(JCas aJCas, String textViewName, JCas view, List<String> lines) throws CASException {
		return aJCas.getView(textViewName);
	}
	
	@Override
	protected void addParallelChunk(JCas textView, int start, String line) {
		return;
	}
	
	
}
