package ca.concordia.clac.evaluation;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Pair;
import org.apache.uima.UIMAException;
import org.apache.uima.cas.CAS;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.CasIOUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.eval.util.ConfusionMatrix;
import org.xml.sax.SAXException;

import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseAnnotationFactory;
import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerMerger;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerToAlignmentFeature;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.DcAlignmentsFeatures;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag;
import ca.concordia.clac.uima.engines.LookupInstanceAnnotator;
import ca.concordia.clac.util.csv.CSVContent;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;

/**
 * Given a CrowdFlower summary report, generate the precision of word alignments.
 * @author majid
 *
 */
public class DcWordAlignmentEvaluation {
	DcAlignmentsFeatures alignmentMapper = new DcAlignmentsFeatures();
	List<Pair<Integer, Integer>> englishDcIntervals = new ArrayList<>();

	public Map<Integer, Pair<Integer, Integer>> alignmentFromXMI(File xmiFile) throws IOException, UIMAException, ParserConfigurationException, SAXException{
		JCas aJCas = JCasFactory.createJCas();
		CasIOUtil.readJCas(aJCas, xmiFile);

		JCas frView = aJCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		
		File frenchDc = File.createTempFile(this.getClass().getSimpleName(), "frenchdc.txt");
		URL lexconnFile = DefaultLexconnReader.getDefaultLexconnFile();
		DefaultLexconnReader lexconn = DefaultLexconnReader.getLexconnMap(lexconnFile);
		FileUtils.writeLines(frenchDc, new ArrayList<>(lexconn.getFormToId().keySet()));
		
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(LookupInstanceAnnotator.getDescription(frenchDc, DiscourseAnnotationFactory.class), CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		SimplePipeline.runPipeline(aJCas, builder.createAggregateDescription());
		
		alignmentMapper.init(frView);
		Collection<DiscourseConnective> frDcs = JCasUtil.select(frView, DiscourseConnective.class);
		Map<Integer, Pair<Integer, Integer>> dcAlignments = new HashMap<>();
		
		for (DiscourseConnective frDc: frDcs){
			List<Token> tokens = alignmentMapper.getAlignedTokens(frDc);
			int alignmentBegin = tokens.size() == 0 ? -1 : tokens.get(0).getBegin();
			int alignmentEnd = tokens.size() == 0 ? -1 : tokens.get(tokens.size() - 1).getEnd();

			dcAlignments.put(frDc.getBegin(), new Pair<Integer, Integer>(alignmentBegin, alignmentEnd));
		}
		
		englishDcIntervals.clear();
		for (DiscourseConnective enDc: JCasUtil.select(aJCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW), DiscourseConnective.class)){
			englishDcIntervals.add(new Pair<Integer, Integer>(enDc.getBegin(), enDc.getEnd()));
		}
		return dcAlignments;
	}
	
	public List<Pair<Integer, Integer>> getEnglishDcIntervals() {
		return englishDcIntervals;
	}
	
	public Map<Integer, Pair<Integer, Integer>> crowdAnnotations(CSVContent fullReport, CSVContent dataset) throws IOException{
		final String crowdAnnotationColumn = "english_marker";
		final String markedColumn = "frchunkbolded";
		final Tag lang = Tag.FR;
		
		dataset = new CrowdFlowerToAlignmentFeature(fullReport, null, crowdAnnotationColumn, markedColumn, lang)
			.mergeCrowdAnnotations(dataset);
		
		CrowdFlowerMerger merger = new CrowdFlowerMerger(dataset);
		merger.fixAndMergeAlignments();
		dataset = merger.getDataset();
		
		dataset.save(new File("outputs/reports/crowd-full-summary.csv"));
		List<Integer> begings = dataset.getColumn(Tag.tagToString(Tag.dc, Tag.begin, Tag.FR)).stream().map(Double::parseDouble).map(Double::intValue).collect(Collectors.toList());
		List<List<Pair<Integer, Integer>>> alignments = dataset.getColumn(Tag.tagToString(Tag.FR, Tag.alignment, Tag.dc, Tag.gold)).stream()
					.map(AlignmentFeature::parse)
					.map(
							(al) -> al.stream()
										.map(a -> new Pair<Integer, Integer>(a.getBegin(), a.getEnd()))
										.collect(Collectors.toList())
						)
					.collect(Collectors.toList());
		
		Map<Integer, Pair<Integer, Integer>> dcAlignments = new HashMap<>();
		for (int i = 0; i < begings.size(); i++){
			dcAlignments.put(begings.get(i), alignments.get(i).get(0));
		}
		return dcAlignments;
	}
	
	public static void main(String[] args) throws IOException, UIMAException, ParserConfigurationException, SAXException {
		File fullReportCsvFile = new File("resources/crowdFlower/emnlp-dataset/full-report-fixed-error.csv");
		File datasetCsvFile = new File("resources/crowdFlower/emnlp-dataset/sampled.csv");
		
		File xmiDir = new File("/Users/majid/Documents/resource/europarl-dataset-17-03-18/xmi");
		DcWordAlignmentEvaluation dcWordAlignmentEvaluation = new DcWordAlignmentEvaluation();
		
		CSVContent dataset = new CSVContent(datasetCsvFile);
		List<String> frDcId = dataset.getColumn(Tag.tagToString(Tag.FR, Tag.dc, Tag.begin));
		List<String> frDc = dataset.getColumn(Tag.tagToString(Tag.FR, Tag.dc, Tag.text));
		Map<Integer, String> dcIdToDc = new HashMap<>();
		DefaultLexconnReader reader = DefaultLexconnReader.getLexconnMap();
		Map<String, String> formToId = reader.getFormToId();
		Map<String, String> idToCanonical = reader.getIdToCanonicalForm();
		for (int i = 0; i < frDcId.size(); i++){
			String id = formToId.get(frDc.get(i).toLowerCase());
			if (id == null){
				System.out.println("DcWordAlignmentEvaluation.main()");
			} 
			dcIdToDc.put((int)Double.parseDouble(frDcId.get(i)), idToCanonical.get(id) + "(" + id + ")");
		}
		
		Map<Integer, Pair<Integer, Integer>> goldAnnotations = dcWordAlignmentEvaluation.crowdAnnotations(new CSVContent(fullReportCsvFile), dataset);
		File dir = new File("outputs/reports");
		Map<String, Tags> nameToTags = new HashMap<>();
		for (File xmiFile: xmiDir.listFiles()){
			Map<Integer, Pair<Integer, Integer>> wordAlignments = dcWordAlignmentEvaluation.alignmentFromXMI(xmiFile);
			
			DCAlignmentEvaluation dcAlignmentEvaluation = new DCAlignmentEvaluation(dcWordAlignmentEvaluation.getEnglishDcIntervals(), dcIdToDc);
			ConfusionMatrix<String> confusionMatrix = dcAlignmentEvaluation.init(goldAnnotations, wordAlignments);
			
			Tags tags = new Tags(dcAlignmentEvaluation.getIdToTag());
			String name = xmiFile.getAbsolutePath();
			System.out.println("DCAlignmentEvaluation.main(): " + name + " = " + tags);
			System.out.println("DCAlignmentEvaluation.main(): " + confusionMatrix.toCSV().replace(',', '\t'));
			PrintWriter pw = new PrintWriter(new File(dir, new File(name).getName() + ".csv"));
			pw.println(name);
			dcAlignmentEvaluation.getDcConfusionMatrix().forEach((dc, cm) -> {
				pw.println(dc);
				pw.println(cm.toCSV());
			});
			
			pw.close();
			nameToTags.put(name, tags);
		}
		
		System.out.println();

		for (String aTags: nameToTags.keySet()){
			for (String bTags: nameToTags.keySet()){
				if (aTags.equals(bTags)){
					continue;
				}

				Tags diff = nameToTags.get(aTags).diff(nameToTags.get(bTags));
				System.out.println("DCAlignmentEvaluation.main(): Diff " + aTags + " with " + bTags);
				System.out.println(diff.getIdToTag());
				System.out.println();
			}
		}

		System.out.println("DcWordAlignmentEvaluation.main()");
		
	}
}
