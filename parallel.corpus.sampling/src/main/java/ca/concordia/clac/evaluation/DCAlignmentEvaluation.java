package ca.concordia.clac.evaluation;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;
import org.cleartk.eval.util.ConfusionMatrix;

import ca.concordia.clac.util.csv.CSVContent;

enum Tag {
	correctZeroTran, incorrectZeroTran,
	correctDCTran, incorrectDCTran,
	correctNDCTran, incorrectNDCTran
}

class Tags{
	Map<String, Tag> idToTag = new HashMap<>();

	public Tags(Map<String, Tag> idToTag) {
		this.idToTag = idToTag;
	}

	public List<String> count(BiPredicate<String, Tag> filter){
		return idToTag.entrySet().stream().filter((e) -> filter.test(e.getKey(), e.getValue())).map(Entry::getKey).collect(Collectors.toList());
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("0.000");
		int total = idToTag.size();
		double acc = (double) count((k, v) -> v == Tag.correctDCTran || v == Tag.correctNDCTran || v == Tag.correctZeroTran).size() / total;
		int totalZeroTrans = count((k, v) -> v == Tag.correctZeroTran || v == Tag.incorrectZeroTran).size();
		double accZeroTrans = (double) count((k, v) -> v == Tag.correctZeroTran).size() / totalZeroTrans;

		int totalDCTrans = count((k, v) -> v == Tag.correctDCTran || v == Tag.incorrectDCTran).size();
		double accDCTrans = (double) count((k, v) -> v == Tag.correctDCTran).size() / totalDCTrans;
		
		int totalNDCTrans = count((k, v) -> v == Tag.correctNDCTran || v == Tag.incorrectNDCTran).size();
		double accNDCTrans = (double) count((k, v) -> v == Tag.correctNDCTran).size() / totalNDCTrans;
		
		return "(" + (int) total + ") Acc = " + df.format(acc) + 
				", Zero Trans (" + (int)totalZeroTrans + ") acc = " + df.format(accZeroTrans) +  
				", TransDC (" + (int)totalDCTrans + ") acc = " + df.format(accDCTrans) + 
				", TransNDC (" + (int)totalNDCTrans + ") acc = " + df.format(accNDCTrans) ;

	}

	public Tags diff(Tags tag){
		Map<String, Tag> diffIdToTag = new HashMap<>();
		idToTag.forEach((k, v) -> {
			if (tag.getIdToTag().get(k) != v)
				diffIdToTag.put(k, v);	
		});
		return new Tags(diffIdToTag);
	}

	public Map<String, Tag> getIdToTag() {
		return idToTag;
	}
}

public class DCAlignmentEvaluation {
	private ConfusionMatrix<String> confusionMatrix;
	private Map<Integer, String> intervalToDcId;
	private Map<String, ConfusionMatrix<String>> dcConfusionMatrix = new HashMap<>();
	public static final String EMPTY_ALIGNMENT = ".";
	private Set<Integer> englishDcPositions = new HashSet<>();

	public DCAlignmentEvaluation(List<Pair<Integer, Integer>> englishDcInterval, Map<Integer, String> intervalToDcId) {
		for (Pair<Integer, Integer> interval: englishDcInterval){
			for (int idx = interval.getFirst(); idx < interval.getSecond(); idx++){
				englishDcPositions.add(idx);
			}
		}
		this.intervalToDcId = intervalToDcId;
		for (String dcId: intervalToDcId.values()){
			dcConfusionMatrix.put(dcId, new ConfusionMatrix<>());
		}
	}

	public DCAlignmentEvaluation() {
		this.englishDcPositions = new HashSet<>();
	}

	Map<String, Tag> idToTag = new HashMap<>();

	public ConfusionMatrix<String> init(Map<Integer, Pair<Integer, Integer>> goldAnnotations, Map<Integer, Pair<Integer, Integer>> systemAnnotations){
		confusionMatrix = new ConfusionMatrix<>();
		
		for (int rowId: goldAnnotations.keySet()){
			Pair<Integer, Integer> gold = goldAnnotations.get(rowId);
			Pair<Integer, Integer> system = systemAnnotations.get(rowId);
			String goldLable = getLabel(gold);
			String systemLable = getLabel(system);
			confusionMatrix.add(goldLable, systemLable);
			dcConfusionMatrix.get(intervalToDcId.get(rowId)).add(goldLable, systemLable);

			if (system == null){
				System.out.println("DCAlignmentEvaluation.init()" + rowId);
			}
			if (gold.getFirst() == -1){
				if (system.getFirst() == -1){
					idToTag.put("" + rowId, Tag.correctZeroTran);
				} else {
					idToTag.put("" + rowId, Tag.incorrectZeroTran);
				}
			} else {
				if (system.getFirst() != -1 && insideOfEachOther(gold, system)){
					idToTag.put("" + rowId, isDc(gold) ? Tag.correctDCTran : Tag.correctNDCTran);
				} else {
					idToTag.put("" + rowId, isDc(gold) ? Tag.incorrectDCTran: Tag.incorrectNDCTran);
				}
			}
		}
		return confusionMatrix;
	}
	
	public Map<String, ConfusionMatrix<String>> getDcConfusionMatrix() {
		return dcConfusionMatrix;
	}

	private String getLabel(Pair<Integer, Integer> gold) {
		String goldLable = "None";
		if (gold.getFirst() != -1)
			goldLable = "" + isDc(gold);
		return goldLable;
	}

	private boolean isDc(Pair<Integer, Integer> gold) {
		boolean result = false;
		for (int idx = gold.getFirst(); idx < gold.getSecond(); idx++){
			result = result || englishDcPositions.contains(idx);
		}
		return result;
	}

	private boolean insideOfEachOther(Pair<Integer, Integer> gold, Pair<Integer, Integer> system) {
		return inside(system, gold) || inside(gold, system);
	}

	private boolean inside(Pair<Integer, Integer> gold, Pair<Integer, Integer> system) {
		return system.getFirst() <= gold.getFirst() && gold.getSecond() <= system.getSecond();
	}

	public void init(List<String> goldAnnotations, List<String> systemAnnotations, List<String> ids){

		for (int i = 0; i < goldAnnotations.size(); i++){
			String gold = goldAnnotations.get(i);
			if (gold.length() > 0){
				String id = ids.get(i);
				String system = systemAnnotations.get(i);

				if (gold.contains(EMPTY_ALIGNMENT)){
					if (system.length() == 0){
						idToTag.put(id, Tag.correctZeroTran);
					} else {
						idToTag.put(id, Tag.incorrectZeroTran);
					}
				} else if ((gold.contains(system) || system.contains(gold)) && system.length() != 0){
					idToTag.put(id, Tag.correctDCTran);
				} else {
					idToTag.put(id, Tag.incorrectDCTran);
				}
			}
		}
	}

	public Map<String, Tag> getIdToTag() {
		return new HashMap<>(idToTag);
	}

	public static void main(String[] args) throws IOException {
		File alignmentFile = new File("resources/crowdFlower/dataset-annotations-reviewed.csv");
		String goldAnnotationColumn = "french_marker_gold";
		String idColumn = "dc_id";

		CSVContent csvContent = new CSVContent(alignmentFile);
		//		new CSVUtils(csvContent).addColumn("merged-en-fr", "fr-en.A3.final", "en-fr.A3.final", (a, b) -> {
		//			String lcs = StringUtils.lcs(a, b);
		//			if (a.contains(lcs) && b.contains(lcs))
		//				return lcs;
		//			return "";
		//		});
		DCAlignmentEvaluation alignmentEvaluation = new DCAlignmentEvaluation();
		Map<String, Tags> nameToTags = new HashMap<>();
		for (String column: csvContent.getHeader()){

			if (column.contains("A3") || column.contains("grow-diag")){
				System.out.println("DCAlignmentEvaluation.main(): " + column);
				List<String> goldAnnotations = csvContent.getColumn(goldAnnotationColumn).stream().map(String::toLowerCase).collect(Collectors.toList());
				List<String> systemAnnotations = csvContent.getColumn(column).stream().map(String::toLowerCase).collect(Collectors.toList());
				List<String> ids = csvContent.getColumn(idColumn);

				alignmentEvaluation.init(goldAnnotations, systemAnnotations, ids);
				Tags tags = new Tags(alignmentEvaluation.getIdToTag());
				System.out.println("DCAlignmentEvaluation.main(): " + column + " = " + tags);
				nameToTags.put(column, tags);
			}
		}

		System.out.println();

		for (String aTags: nameToTags.keySet()){
			for (String bTags: nameToTags.keySet()){
				if (aTags.equals(bTags)){
					continue;
				}

				Tags diff = nameToTags.get(aTags).diff(nameToTags.get(bTags));
				System.out.println("DCAlignmentEvaluation.main(): Diff " + aTags + " with " + bTags);
				System.out.println(diff.getIdToTag());
				System.out.println();
			}
		}

		System.out.println(nameToTags.get("A3.final").count((k, v) -> v == Tag.incorrectZeroTran));
	}
}
