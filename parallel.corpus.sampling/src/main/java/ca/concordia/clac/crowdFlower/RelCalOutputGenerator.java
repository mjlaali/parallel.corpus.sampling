package ca.concordia.clac.crowdFlower;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import ca.concordia.clac.util.csv.CSVContent;

/** 
 * This file generate a csv file for the RelCal website (http://dfreelon.org/utils/recalfront/recal3/) from CrowdFlower summary file in order to calculate the agreement between 
 * annotators.
 * @author majid
 *
 */
public class RelCalOutputGenerator {
	CrowdAnnotatorPartitionar crowdAnnotatorPartitionar;
	
	public RelCalOutputGenerator(final List<String> rowIds, final List<String> annotatorIds, final List<String> annotations, int groupCnt) {
		crowdAnnotatorPartitionar = new CrowdAnnotatorPartitionar(rowIds, annotatorIds, annotations, groupCnt);
		System.err.println("RelCalOutputGenerator.RelCalOutputGenerator(): groups = " + crowdAnnotatorPartitionar.getGroups());
	}

	public List<List<Integer>> analysis(){
		List<String> annotators = new ArrayList<>();

		List<Map<String, String>> rows = new ArrayList<>();
		Set<String> missedRows = crowdAnnotatorPartitionar.getMissedRows();
		Map<String, String> annotatorToGroup = new HashMap<>();
		
		int idx = 0;
		for(Set<String> aGroup: crowdAnnotatorPartitionar.getGroups()){
			final String groupIdx = "" + idx;
			annotators.add(groupIdx);
			aGroup.forEach((a) -> annotatorToGroup.put(a, groupIdx));
			++idx;
		}
		
		for (Entry<String, Map<String, String>> rowAnnotations: crowdAnnotatorPartitionar.getRowToAnnotations().entrySet()){
			if (missedRows.contains(rowAnnotations.getKey())){
				Map<String, String> groupToAnnotations = new HashMap<>();
				rowAnnotations.getValue().forEach((annotator, annotation) -> groupToAnnotations.put(annotatorToGroup.get(annotator), pruned(annotation)));
				if (groupToAnnotations.size() == crowdAnnotatorPartitionar.getGroupCnt())
					rows.add(groupToAnnotations);
				else
					missedRows.add(rowAnnotations.getKey());
			}
		}
		System.err.println("RelCalOutputGenerator.RelCalOutputGenerator(): Missed rows = " + missedRows.size() + "\n" + missedRows);

		Map<String, Integer> value2Id = new TreeMap<>();
		
		List<List<Integer>> output = new ArrayList<>();
		for (Map<String, String> aRow: rows){
			List<String> values = new ArrayList<>(aRow.values());
			
			for (Entry<String, String> annValue: new HashSet<>(aRow.entrySet())){
				String updatedValue = annValue.getValue();
				for (String biggerValue: values){
					if (biggerValue.contains(updatedValue)){
						updatedValue = biggerValue;
					}
				}
				Integer valueId = value2Id.get(updatedValue);
				if (valueId == null){
					valueId = value2Id.size();
					value2Id.put(updatedValue, valueId);
				}
				aRow.put(annValue.getKey(), updatedValue);
			}

			List<Integer> valueIdxes = new ArrayList<>();
			for (String user: annotators){
				String value = aRow.get(user);
				int valueIdx = -1;
				if (value != null)
					valueIdx = value2Id.get(value);
				valueIdxes.add(valueIdx);
			}
			
			output.add(valueIdxes);
		}
		
		System.out.println(value2Id);

		return output;
	}

	private String pruned(String annotation) {
		return annotation.replaceAll("\\d+", "");
	}

	public static void main(String[] args) throws IOException {
		File csvFile = new File("resources/crowdFlower/emnlp-dataset/full-report-without-gold-tests.csv");
		String annotationColumn = "english_marker";
		String annotatorIdColumn = "_worker_id";
		String rowIdColumn = "_unit_id";
		
		CSVContent content = new CSVContent(csvFile);

		RelCalOutputGenerator generator = new RelCalOutputGenerator(
				content.getColumn(rowIdColumn), content.getColumn(annotatorIdColumn), content.getColumn(annotationColumn),
				3);
		List<List<Integer>> analysis = generator.analysis();
		for (List<Integer> aRow: analysis){
			String result = aRow.stream().map((i) -> i.toString()).collect(Collectors.joining(","));
			System.out.println(result);
		}
	}
}
