package ca.concordia.clac.crowdFlower;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CrowdAnnotatorPartitionar {
	final Map<String, Map<String, String>> rowToAnnotations = new HashMap<>();
	final Set<String> annotatorIds = new HashSet<>();
	final Map<String, Set<String>> annotatorToRowAnnotations = new HashMap<>();
	final List<Set<String>> groups = new ArrayList<>();
	final Set<String> missedRows = new HashSet<>();
	int groupCnt;
	
	public CrowdAnnotatorPartitionar(final List<String> rowIds, final List<String> annotatorIds, final List<String> annotations, int groupCnt) {
		this.groupCnt = groupCnt;
		init(rowIds, annotatorIds, annotations);
		divideAnnotatorsTo(groupCnt);
	}
	
	private void init(final List<String> rowIds, final List<String> annotatorIds, final List<String> annotations){
		if (rowIds.size() != annotatorIds.size() || rowIds.size() != annotations.size())
			throw new RuntimeException("The number of rows are not equal");
		
		this.annotatorIds.addAll(annotatorIds);
		HashSet<String> uniquRowIds = new HashSet<>(rowIds);
		for (String rowId: uniquRowIds){
			rowToAnnotations.put(rowId, new HashMap<>());
		}
		
		for (String annotatorId: annotatorIds){
			annotatorToRowAnnotations.put(annotatorId, new HashSet<>());
		}
		
		for (int i = 0; i < rowIds.size(); i++){
			if (rowToAnnotations.get(rowIds.get(i)).put(annotatorIds.get(i), annotations.get(i)) != null)
				throw new RuntimeException("Multplie annotation for the same row <" + rowIds.get(i) + "> from the same annotator <" + annotatorIds.get(i) + ">");
			annotatorToRowAnnotations.get(annotatorIds.get(i)).add(rowIds.get(i));
		}
		
		Set<String> invalidRows = new HashSet<>();
		for (String rowId: uniquRowIds){
			if (rowToAnnotations.get(rowId).size() < groupCnt){
				invalidRows.add(rowId);
			};
		}
		remove(invalidRows);
	}
	
	private void divideAnnotatorsTo(int groupCnt){
		for (int i = 0; i < groupCnt; i++){
			groups.add(new HashSet<>());
		}
		
		while (!annotatorIds.isEmpty()) {
			String annotator = pickAnnotatorWithMostAnnotations();
			int minLost = Integer.MAX_VALUE;
			int bestGroup = -1;
			for (int i = 0; i < groupCnt; i++){
				int lost = missedRow(annotator, groups.get(i)).size();
				if (lost < minLost){
					minLost = lost;
					bestGroup = i;
				}
			}
			
			remove(missedRow(annotator, groups.get(bestGroup)));
			groups.get(bestGroup).add(annotator);
			annotatorIds.remove(annotator);
		}
		
	}
	
	
	private void remove(Set<String> missedRow) {
		this.missedRows.addAll(missedRow);
		for (String row: missedRow){
			for (String anAnnotator: rowToAnnotations.get(row).keySet()){
				annotatorToRowAnnotations.get(anAnnotator).remove(row);
			}
		}
	}

	private Set<String> missedRow(String annotator, Set<String> setOfAnnotators) {
		Set<String> unions = new TreeSet<>();
		for (String anAnnotator: setOfAnnotators){
			unions.addAll(annotatorToRowAnnotations.get(anAnnotator));
		}
		
		unions.retainAll(annotatorToRowAnnotations.get(annotator));
		return unions;
	}


	private String pickAnnotatorWithMostAnnotations() {
		int max = -1;
		String maxAnnotator = null;
		for (String anAnnotator: annotatorIds){
			if (annotatorToRowAnnotations.get(anAnnotator).size() > max){
				max = annotatorToRowAnnotations.get(anAnnotator).size();
				maxAnnotator = anAnnotator;
			}
		}
		return maxAnnotator;
	}
	
	public int getMissedRowCnt() {
		return missedRows.size();
	}
	
	public Set<String> getMissedRows() {
		return missedRows;
	}
	
	public List<Set<String>> getGroups() {
		return groups;
	}
	
	public Map<String, Map<String, String>> getRowToAnnotations() {
		return rowToAnnotations;
	}
	
	public int getGroupCnt() {
		return groupCnt;
	}
}
